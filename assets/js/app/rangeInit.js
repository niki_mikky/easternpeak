$(function() {

    var $document = $(document);
    var selector = '.rangeslider';
    var $element = $(selector);
    var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

    $element.rangeslider({
        polyfill: false
    });

    function appendResult() {
        let valueDiv = document.createElement('div');
        let dragDiv = document.createElement('div');

        valueDiv.classList.add('result');
        dragDiv.classList.add('drag');

        const drag = 'Drag';
        dragDiv.innerHTML = drag;

        
        let handler = document.getElementsByClassName('rangeslider__handle')[0];
        handler.insertBefore(valueDiv, null);
        handler.insertBefore(dragDiv, null);

        let currentValue = document.getElementsByClassName('rangeslider')[0].value;
        document.getElementsByClassName("result")[0].innerHTML = currentValue;
    };
    appendResult();

    function valueOutput(element) {
        let value = element.value;
        let output = element.parentNode.getElementsByClassName('result')[0] || element.parentNode.parentNode.getElementsByClassName('result')[0];
        output[textContent] = value;
    }

    $document.on('input', 'input[type="range"], ' + selector, function(e) {
        valueOutput(e.target);
    });
});