$(document).ready(function(){
    //Variables
    let status = $('.pageInfo');
    let content = $('.carouselContent');
    let nav = $('.carouselNav')

    //Identify current slide
    content.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        let i = (currentSlide ? currentSlide : 0) + 1;
        status.text(i + '/' + slick.slideCount);
    });
    
    //Nav init
    nav.slick({
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 6,
        asNavFor: '.carouselContent',
        dots: false,
        focusOnSelect: true,
        responsive: [
            {
              breakpoint: 1399,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 980,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
              }
            }
          ]
    });			

    //Content init
    content.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        appendArrows: $('.navigation'),
        fade: true,
        asNavFor: '.carouselNav'
    });
});